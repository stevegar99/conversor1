/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import View.Calculadora;
import View.Panel1;
import View.Panel2;
import View.VentanaPrincipal;

/**
 *
 * @author Bismark Steve Garcia
 */
public class Coordinador {
    
    Panel1 miPanel1;
    Panel2 miPanel2;
    Logica miLogica;
    VentanaPrincipal miVentana;
    Calculadora miCalculadora;
    
    public int Convertir(String num1, String num2) {
        return miLogica.Convertir(num1, num2);
    }

    public String Convertir(int num1, int num2) {
        System.out.println(num1);
        System.out.println(num2);
        return miLogica.Convertir(num1, num2);
    }
        
    public void setPanel1(Panel1 miPanel1) {
        this.miPanel1 = miPanel1;
    }

    public void setPanel2(Panel2 miPanel2) {
        this.miPanel2 = miPanel2;
    }

    public void setLogica(Logica miLogica) {
        this.miLogica = miLogica;
    }

    public void setVentanaPrincipal(VentanaPrincipal miVentana) {
        this.miVentana = miVentana;
    }

    public void cargarPanel(int panel) {
        switch(panel){
            case 1:
                miVentana.definirPanel(miPanel1);
                break;
            case 2:
                miVentana.definirPanel(miPanel2);
                break;
            case 3:
                miCalculadora = new Calculadora();
                miCalculadora.setVisible(true);
            default:
                break;
        }
            
    }

    public void setCalculadora(Calculadora miCalculadora) {
        this.miCalculadora = miCalculadora;
    }
}
