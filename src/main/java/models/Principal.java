/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import Controllers.Coordinador;
import Controllers.Logica;
import View.Calculadora;
import View.Panel1;
import View.Panel2;
import View.VentanaPrincipal;

/**
 *
 * @author Bismark Steve Garcia
 */
public class Principal {
     public static void main(String[] args){
        Coordinador miCoordinador = new Coordinador();
        Panel1 miPanel1 = new Panel1();
        Panel2 miPanel2 = new Panel2();
        Logica miLogica = new Logica();
        VentanaPrincipal miVentana = new VentanaPrincipal();
        Calculadora miCalculadora = new Calculadora();
        
        miPanel1.setCoordinador(miCoordinador);
        miPanel2.setCoordinador(miCoordinador);
        miLogica.setCoordinador(miCoordinador);
        miVentana.setCoordinador(miCoordinador);
        miCalculadora.setCoordinador(miCoordinador);
        
        miCoordinador.setPanel1(miPanel1);
        miCoordinador.setPanel2(miPanel2);
        miCoordinador.setLogica(miLogica);
        miCoordinador.setVentanaPrincipal(miVentana);
        miCoordinador.setCalculadora(miCalculadora);
        
        miCalculadora.setVisible(true);
    }
    
}


